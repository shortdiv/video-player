var $scrub = $('#scrubber'),
    $scrubbar = $('#scrub-bar'),
    $progress = $('#progress'),
    offset = $scrubbar.offset().left,
    scrubbarWidth = $scrubbar.width(),
    scrubWidth = $scrub.width(),
    timeDrag = false;

$scrub.bind("mousedown", function(evt) {
  evt.preventDefault();
  timeDrag = true;
});

$scrub.bind("mouseup", function(evt) {
  evt.preventDefault();
  if(timeDrag){
    timeDrag = !timeDrag; //set to false
  }
});

$scrubbar.bind("click", function(evt){
  updateScrub(evt);
});

$(document).mousemove(function(evt){
    evt.preventDefault();
  if(timeDrag){
    updateScrub(evt);
  }
});

$(document).bind("mouseup", function(evt){
  if(timeDrag){
    timeDrag = false;
    $scrub.mouseup();
  }
});

function updateScrub(e) {
  var mousePos = e.pageX;

  relPos = mousePos - offset;

  //check within range
  maintainRangeConstraint(relPos);

  fraction = relPos/scrubbarWidth;

  $scrub.css({left: "" + (relPos + scrubWidth) + "px"});
  updateProgress(fraction);
}

function updateProgress(num){
  $progress.width((num*100) + "%");
}

function maintainRangeConstraint(relPos) {
  //return true or false

  if(relPos >= scrubbarWidth){
    $scrub.mouseup();
    relPos = scrubbarWidth;
    return relPos;
  } else if (relPos <= 0) {
    $scrub.mouseup();
    relPos = offset;
    return relPos;
  }
}

