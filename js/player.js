var video = document.getElementById('video'),
    seekBar = document.getElementById('seekbar'),
    counter = document.getElementById('counter'),
    $video = $('#video'),
    $scrubbar = $('#scrub-bar'),
    $scrub = $('#scrubber'),
    $progress = $('#progress'),
    offset = $scrubbar.offset().left,
    scrubbarWidth = $scrubbar.width(),
    scrubWidth = $scrub.width()
    timeDrag = false;

$('.play_border, #video').click(function(){
  if(video.paused === true) {
    video.play();  
  } else {
    video.pause();
  }
});

$video.bind("timeupdate", videoTimeUpdateHandler);
$scrubbar.bind("click", scrubbarMouseDownHandler);

function videoTimeUpdateHandler(e) {
  var videoDom = $video.get(0),
      fraction = video.currentTime/video.duration;
  updateProgress(fraction);
  updateScrubPos(fraction);
}

function scrubbarMouseDownHandler(e) {
  var $this = $(this);
  var xPos = e.pageX - offset;
  var watched = xPos / scrubbarWidth;
  updateProgress(watched);
  updateVideoTime(watched);
}

///////// SCRUBBAR ////////

///////// SCRUBBAR ////////

//Helper Functions
function roundUp(time) {
  return Math.round(time*100)/100;
}

function updateScrubPos(num){
  var move = num * scrubbarWidth + offset;
  $scrub.css({left: "" + move + "px"});
}

function updateProgress(num){
  $progress.width((num*100) + "%");
}

function updateVideoTime(fracWatched) {
  var video = $video.get(0);
    video.currentTime = fracWatched * video.duration;
}

//updateTimerText//
video.addEventListener("playing", function() {
  timer = setInterval(printifPlaying, 1000);
});

function printifPlaying() {
  if(!video.paused) {
    var roundedMin = 0,
        roundedSec = Math.floor(video.currentTime);

    if(roundedSec > 59){
      roundedMin = Math.floor(roundedSec/60);
      remainingSec = (roundedSec % 60);
      roundedSec = remainingSec;
    }
    time = leadingZero(roundedMin) + ":" + leadingZero(roundedSec);
    counter.innerHTML = time;
    console.log(time)
  } else {
    clearInterval(timer);
  }
}

function leadingZero(num){
  if(num < 10) {
    return "0" + num;
  } else {
    return num;
  }
}

